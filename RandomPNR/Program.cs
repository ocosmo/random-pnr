﻿using System;
using System.Text;
using System.Threading;
using System.Text.RegularExpressions;
using System.IO;
using System.Windows.Forms;
using System.Reflection;
using System.Globalization;

namespace RandomPNR {

    public enum DateInterval {
        Day,
        DayOfYear,
        Hour,
        Minute,
        Month,
        Quarter,
        Second,
        Weekday,
        WeekOfYear,
        Year
    }

    public class Program {
        // Fields
        private static DateTime date;
        private static bool divider = true;
        private static string end_date = "3000";
        private static string format = "yyyyMMdd";
        private static string start_date = "1000";

        // Methods
        private static int CalculatePNRChecksum(int PNRValue) {
            int num = 10 - (PNRValue % 10);
            if (num == 10) {
                num = 0;
            }
            return num;
        }

        private static int CalculatePNRValue(string date_string) {
            int num = 0;
            for (int i = 0; i < date_string.Length; i++) {
                int num3 = (date_string[i] - 0x30) << (1 - (i & 1));
                if (num3 > 9) {
                    num3 -= 9;
                }
                num += num3;
            }
            return num;
        }

        private static int CalculatePNRValue(DateTime date, string last_three) {
            string str = date.ToString("yyMMdd") + last_three;
            int num = 0;
            for (int i = 0; i < str.Length; i++) {
                int num3 = (str[i] - 0x30) << (1 - (i & 1));
                if (num3 > 9) {
                    num3 -= 9;
                }
                num += num3;
            }
            return num;
        }

        private static void Generate(string[] commands) {
            string str;
            int num = 0;
            if (commands.Length > 1) {
                try {
                    num = int.Parse(commands[1]);
                    if (num < 1) {
                        num = 1;
                    }
                    goto Label_0021;
                } catch (FormatException) {
                    return;
                }
            }
            num = 1;
        Label_0021:
            str = "";
            for (int i = 0; i < num; i++) {
                str = str + getRandomValidPNR(new DateTime(int.Parse(start_date), 1, 1), new DateTime(int.Parse(end_date), 12, 0x1f), format);
                if ((num > 1) && ((i + 1) != num)) {
                    str = str + "\r\n";
                    Thread.Sleep(30);
                }
            }
            Clipboard.SetText(str);
            Print(string.Concat(new object[] { num, " person# mellan ", start_date, " - ", end_date, " kopierat till urklipp!" }));
        }

        private static void GenerateFromDate() {
            Print("Det var ett datum! Ange TRE av de fyra sista siffrorna:");
            string input = Console.ReadLine();
            Regex regex = new Regex(@"\b[0-9]{3}\b");
            if (regex.IsMatch(input)) {
                int pNRValue = CalculatePNRValue(date, input);
                string text = "";
                if (divider) {
                    text = string.Concat(new object[] { date.ToString(format), "-", input, CalculatePNRChecksum(pNRValue) });
                }
                else {
                    text = date.ToString(format) + input + CalculatePNRChecksum(pNRValue);
                }
                Clipboard.SetText(text);
                Print("person# " + text + " kopierat till urklipp!");
            }
            else {
                Print("Du angav inte korrekt data.");
            }
        }

        private static string GetInstructions() {
            try {
                using (StreamReader reader = new StreamReader(Assembly.GetExecutingAssembly().GetManifestResourceStream("RandomPNR.instructions.txt"))) {
                    return reader.ReadToEnd();
                }
            }catch(ArgumentNullException) {
                return "Hjälptexten kunde inte visas. Prova att starta om programmet.";
            }
        }

        private static DateTime getRandomDate(Random rand, DateTime minDate, DateTime maxDate) {
            int maxValue = (int)DateTimeUtil.DateDiff(DateInterval.Day, minDate, maxDate);
            int num2 = rand.Next(0, maxValue);
            return minDate.AddDays((double)num2);
        }

        private static string getRandomValidPNR(DateTime minDate, DateTime maxDate, string date_format) {
            Random rand = new Random();
            DateTime date = getRandomDate(rand, minDate, maxDate);
            int num = 0;
            string str = "";
            while (num < 3) {
                str = str + rand.Next(0, 10).ToString();
                num++;
            }
            int num3 = CalculatePNRChecksum(CalculatePNRValue(date, str));
            if (divider) {
                return string.Concat(new object[] { date.ToString(date_format), "-", str, num3 });
            }
            return (date.ToString(date_format) + str + num3);
        }

        private static string GetStartmessage() {
            Assembly assembly = Assembly.GetExecutingAssembly();
            System.Diagnostics.FileVersionInfo fvi = System.Diagnostics.FileVersionInfo.GetVersionInfo(assembly.Location);
            string version = fvi.FileVersion;
            return string.Format("\nDet h\x00e4r \x00e4r version {0} av pnrGen\nSkapad av Oscar Cosmo\n\nSkriv 'help' f\x00f6r instruktioner\n", version);
        }

        public static bool IsDate(string input) {
            return ((((DateTime.TryParse(input, CultureInfo.CurrentCulture, DateTimeStyles.NoCurrentDateDefault, out date) && (date.Hour == 0)) && ((date.Minute == 0) && (date.Second == 0))) && (date.Millisecond == 0)) && (date > DateTime.MinValue));
        }

        [STAThread]
        private static void Main(string[] args) {
            Console.Write(GetStartmessage());
        Label_000A:
            Console.Write("\n$");
            string[] commands = Console.ReadLine().Split(new char[] { ' ' });
            if (commands[0].ToLower().CompareTo("from").Equals(0)) {
                SetFrom(commands);
                goto Label_000A;
            }
            if (commands[0].ToLower().CompareTo("to").Equals(0)) {
                SetTo(commands);
                goto Label_000A;
            }
            if (commands[0].ToLower().CompareTo("gen").Equals(0)) {
                Generate(commands);
                goto Label_000A;
            }
            if (commands[0].ToLower().CompareTo("format").Equals(0)) {
                SetFormat(commands);
                goto Label_000A;
            }
            if (commands[0].ToLower().CompareTo("div").Equals(0)) {
                divider = true;
                goto Label_000A;
            }
            if (commands[0].ToLower().CompareTo("nodiv").Equals(0)) {
                divider = false;
                goto Label_000A;
            }
            if (commands[0].ToLower().CompareTo("help").Equals(0)) {
                Print(GetInstructions());
                goto Label_000A;
            }
            if (commands[0].ToLower().CompareTo("exit").Equals(0)) {
                return;
            }
            if (IsDate(commands[0])) {
                GenerateFromDate();
            }
            goto Label_000A;
        }

        private static void Print(string text) {
            Console.WriteLine(text);
        }

        private static void SetFormat(string[] commands) {
            if (commands.Length > 1) {
                DateTime.Today.ToString(commands[1]);
                format = commands[1];
            }
            Print("Dagens datum enligt angivet format: " + DateTime.Today.ToString(format));
        }

        private static void SetFrom(string[] commands) {
            if (commands.Length > 1) {
                Regex regex = new Regex(@"\d{4}");
                if (regex.IsMatch(commands[1]) && (int.Parse(commands[1]) <= int.Parse(end_date))) {
                    start_date = commands[1];
                    Print("Start\x00e5r satt till " + start_date);
                }
                else {
                    Print("Kunde inte s\x00e4tta start\x00e5r");
                }
            }
            else {
                Print("Start\x00e5r \x00e4r: " + start_date);
            }
        }

        private static void SetTo(string[] commands) {
            if (commands.Length > 1) {
                Regex regex = new Regex(@"\d{4}");
                if (regex.IsMatch(commands[1]) && (int.Parse(commands[1]) >= int.Parse(start_date))) {
                    end_date = commands[1];
                    Print("Slut\x00e5r satt till " + end_date);
                }
                else {
                    Print("Kunde inte s\x00e4tta slut\x00e5r");
                }
            }
            else {
                Print("Slut\x00e5r \x00e4r: " + end_date);
            }
        }
    }
}
 

