﻿using System;

namespace RandomPNR {

    public sealed class DateTimeUtil {
        // Methods
        public static long DateDiff(DateInterval interval, DateTime dt1, DateTime dt2) {
            return DateDiff(interval, dt1, dt2, System.Globalization.DateTimeFormatInfo.CurrentInfo.FirstDayOfWeek);
        }

        public static long DateDiff(DateInterval interval, DateTime dt1, DateTime dt2, DayOfWeek eFirstDayOfWeek) {
            if (interval == DateInterval.Year) {
                return (long)(dt2.Year - dt1.Year);
            }
            if (interval == DateInterval.Month) {
                return (long)((dt2.Month - dt1.Month) + (12 * (dt2.Year - dt1.Year)));
            }
            TimeSpan span = (TimeSpan)(dt2 - dt1);
            if ((interval == DateInterval.Day) || (interval == DateInterval.DayOfYear)) {
                return Round(span.TotalDays);
            }
            if (interval == DateInterval.Hour) {
                return Round(span.TotalHours);
            }
            if (interval == DateInterval.Minute) {
                return Round(span.TotalMinutes);
            }
            if (interval == DateInterval.Second) {
                return Round(span.TotalSeconds);
            }
            if (interval == DateInterval.Weekday) {
                return Round(span.TotalDays / 7.0);
            }
            if (interval == DateInterval.WeekOfYear) {
                while (dt2.DayOfWeek != eFirstDayOfWeek) {
                    dt2 = dt2.AddDays(-1.0);
                }
                while (dt1.DayOfWeek != eFirstDayOfWeek) {
                    dt1 = dt1.AddDays(-1.0);
                }
                span = (TimeSpan)(dt2 - dt1);
                return Round(span.TotalDays / 7.0);
            }
            if (interval == DateInterval.Quarter) {
                double quarter = GetQuarter(dt1.Month);
                double num2 = GetQuarter(dt2.Month);
                double num3 = num2 - quarter;
                double num4 = 4 * (dt2.Year - dt1.Year);
                return Round(num3 + num4);
            }
            return 0L;
        }

        private static int GetQuarter(int nMonth) {
            if (nMonth <= 3) {
                return 1;
            }
            if (nMonth <= 6) {
                return 2;
            }
            if (nMonth <= 9) {
                return 3;
            }
            return 4;
        }

        private static long Round(double dVal) {
            if (dVal >= 0.0) {
                return (long)Math.Floor(dVal);
            }
            return (long)Math.Ceiling(dVal);
        }
    }
}