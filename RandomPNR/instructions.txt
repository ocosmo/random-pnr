﻿	gen [{num}]		Genererar ett slumpat personnummer mellan 
				intervallen som är satta för start och slutår. 
				{num} anges som heltal och resulterar i att 
				{num} antal slumpade personnummer genereras.
				Genererat/de personnummer lägg direkt i 
				urklipp, redo att klistras in.
					
	to [{year}]		Sätter undre gräns för årtal. 
				Anges inget årtal visas aktuellt värde.
	
	from [{year}]		Sätter övre gräns för årtal. 
				Anges inget årtal visas aktuellt värde.
	
	div / nodiv		Togglar mellan att visa streck innan fyra
				sista siffrorna.
				
	format [{format}]	Sätter aktuellt datumformat. Om inget
				format anges visas aktuellt format.
	
	exit			Avslutar programmet.
	
	help			Visar den här texten.
	
	Also, given a date (YYYY-mm-dd) the program will generate the checknumber.